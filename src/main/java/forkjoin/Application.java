package forkjoin;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.stream.LongStream;

public class Application {

    public static void main(String[] args) {
        long[] numbers = LongStream.rangeClosed(1, 1_000_000_000).toArray();
        ForkJoinTask<Long> task = new ForkJoinSumCalculator(numbers, 0, numbers.length - 1);
        new ForkJoinPool().invoke(task);
    }
}
