package evolvingapi;

public interface Drawable {

    void draw();
}
