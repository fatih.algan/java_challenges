package lambdas;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class LambdaExercises {

    public static void main(String[] args) {

        //FlatMap
        List<Integer> numbers1 = Arrays.asList(1, 2, 3);
        final List<Integer> numbers2 = Arrays.asList(3, 4);
        numbers1.stream()
                .flatMap(i -> numbers2.stream().map(k ->
                            new int[] {i, k}
                        )
                ).forEach(arr -> System.out.println("{" + arr[0] + ", " + arr[1] + "}"));


        //Flatten List of options
        List<Optional<String>> optionalValues = Arrays.asList(Optional.of("a"), Optional.empty(), Optional.of("b"));
        List<String> values = optionalValues.stream().flatMap(opt -> opt.isPresent() ? Stream.of(opt.get()) : Stream.empty())
                .collect(Collectors.toList());


        //Pythegorian Triples
        Stream<int[]> pythegorianTriples =
                IntStream.rangeClosed(1, 100).boxed()
                .flatMap(a ->
                    IntStream.rangeClosed(a, 100)
                        .mapToObj(b -> new double[]{a, b, Math.sqrt(a*a + b*b)})
                        .filter(i -> i[2] % 1 == 0)

                ).map(k -> Arrays.stream(k).mapToInt(z -> (int)z).toArray());
        pythegorianTriples.forEach(arr -> System.out.printf("{ %d, %d, %d} ", arr[0], arr[1], arr[2]));


        //Stream from File
        long uniqueWords = 0;
        try(Stream<String> lines = Files.lines(Paths.get("data.txt"), Charset.defaultCharset())) {
            uniqueWords = lines.flatMap(line -> Arrays.stream(line.split(" ")))
                  .distinct()
                  .count();
        } catch(IOException e) {

        }

        //Fibonacci
        Stream.iterate(new int[] {0, 1}, a -> new int[]{ a[1], a[0] + a[1] })
            .limit(20)
            .forEach(arr -> System.out.println("(" + arr[0] + "," + arr[1] +")"));


        Trader raoul = new Trader("Raoul", "Cambridge");
        Trader mario = new Trader("Mario","Milan");
        Trader alan = new Trader("Alan","Cambridge");
        Trader brian = new Trader("Brian","Cambridge");
        List<Transaction> transactions = Arrays.asList(
                new Transaction(brian, 2011, 300, Currency.getInstance(Locale.ENGLISH)),
                new Transaction(raoul, 2012, 1000, Currency.getInstance(Locale.ENGLISH)),
                new Transaction(raoul, 2011, 400, Currency.getInstance(Locale.ENGLISH)),
                new Transaction(mario, 2012, 710, Currency.getInstance(Locale.CANADA)),
                new Transaction(mario, 2012, 700, Currency.getInstance(Locale.CANADA)),
                new Transaction(alan, 2012, 950, Currency.getInstance(Locale.CHINA))
        );

        Map<Currency, List<Transaction>> transactionByCurrencies =  transactions.stream()
                .collect(Collectors.groupingBy(Transaction::getCurrency));

        List<Dish> menu = Dish.menu;
        menu.stream().collect(Collectors.counting());
        menu.stream().count();

        Comparator<Dish> dishCaloriesComparator = Comparator.comparingInt(Dish::getCalories);
        Optional<Dish> maybeMaxCaloryDish = menu.stream().collect(Collectors.maxBy(dishCaloriesComparator));
        int totalCalories = menu.stream().collect(Collectors.summingInt(Dish::getCalories));
        double avgCalories = menu.stream().collect(Collectors.averagingDouble(Dish::getCalories));
        IntSummaryStatistics menuStatistics = menu.stream().collect(Collectors.summarizingInt(Dish::getCalories));

        String shortMenu = menu.stream().map(Dish::getName).collect(Collectors.joining(", "));

        totalCalories = menu.stream().collect(Collectors.reducing(0, Dish::getCalories, (acc, cal) -> acc + cal));
        totalCalories = menu.stream().map(Dish::getCalories).collect(Collectors.reducing(0, (acc, cal) -> acc + cal));

        Optional<Dish> mostCalorieDish = menu.stream().collect(Collectors.reducing((d1, d2) -> (d1.getCalories() > d2.getCalories()) ? d1 : d2));

        Map<Dish.Type, List<Dish>> dishesByType = menu.stream().collect(Collectors.groupingBy(Dish::getType));

        Map<CaloricLevel, List<Dish>> dishesByCaloricLevel = menu.stream().collect(Collectors.groupingBy(dish -> {
            if (dish.getCalories() <= 400) return CaloricLevel.DIET;
            else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
            else return CaloricLevel.FAT;
        }));

        Map<Dish.Type, List<String>> dishNamesByType = menu.stream().collect(Collectors.groupingBy(Dish::getType,
                Collectors.mapping(Dish::getName, Collectors.toList())));

        Map<String, List<String>> dishTags = new HashMap<>();
        dishTags.put("pork", Arrays.asList("greasy", "salty"));
        dishTags.put("beef", Arrays.asList("salty", "roasted"));
        dishTags.put("chicken", Arrays.asList("fried", "crisp"));
        dishTags.put("french fries", Arrays.asList("greasy", "fried"));
        dishTags.put("rice", Arrays.asList("light", "natural"));
        dishTags.put("season fruit", Arrays.asList("fresh", "natural"));
        dishTags.put("pizza", Arrays.asList("tasty", "salty"));
        dishTags.put("prawns", Arrays.asList("tasty", "roasted"));
        dishTags.put("salmon", Arrays.asList("delicious", "fresh"));

        Map<Dish.Type, Set<String>> dishNamesByType2 = menu.stream().collect(
                Collectors.groupingBy(Dish::getType,
                        Collectors.flatMapping(dish -> dishTags.get(dish.getName()).stream(), Collectors.toSet()))
        );

        //Multilevel Grouping
        Map<Dish.Type, Map<CaloricLevel, List<Dish>>> dishesByTypeCaloricLevel = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.groupingBy(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                }))
        );

        Map<Dish.Type, Long> typesCount = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.counting())
        );


        Map<Dish.Type, Optional<Dish>> mostCaloricByType = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)))
        );

        Map<Dish.Type, Dish> mostCaloricByType2 = menu.stream().collect(
                Collectors.groupingBy(Dish::getType,
                        Collectors.collectingAndThen(
                            Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)), Optional::get)
                )
        );

        Map<Dish.Type, Integer> totalCaloriesByType = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.summingInt(Dish::getCalories))
        );

        Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.mapping(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                }, Collectors.toSet()))
        );

        Map<Dish.Type, Set<CaloricLevel>> caloricLevelsByType2 = menu.stream().collect(
                Collectors.groupingBy(Dish::getType, Collectors.mapping(dish -> {
                    if (dish.getCalories() <= 400) return CaloricLevel.DIET;
                    else if (dish.getCalories() <= 700) return CaloricLevel.NORMAL;
                    else return CaloricLevel.FAT;
                }, Collectors.toCollection(HashSet::new)))
        );


        //Partitioning
        Map<Boolean, List<Dish>> partitionedMenu = menu.stream().collect(
                Collectors.partitioningBy(Dish::isVegetarian)
        );

        Map<Boolean, Map<Dish.Type, List<Dish>>> vegetarianDishesByType = menu.stream()
                .collect(Collectors.partitioningBy(Dish::isVegetarian,
                        Collectors.groupingBy(Dish::getType)));

        Map<Boolean, Dish> mostCaloricPartitionedByVegetarian = menu.stream()
                .collect(Collectors.partitioningBy(Dish::isVegetarian,
                        Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparingInt(Dish::getCalories)),
                                Optional::get)
                        )
                );

        Map<Boolean, Optional<Dish>> mostCaloricPartitionedByVegetarian2 = menu.stream()
                .collect(Collectors.partitioningBy(Dish::isVegetarian,
                        Collectors.maxBy(Comparator.comparingInt(Dish::getCalories))));


        //Partition numbers according to being a prime or not
        Map<Boolean, List<Integer>> primes = IntStream.rangeClosed(1, 100).boxed()
                .collect(Collectors.partitioningBy(LambdaExercises::isPrime));

    }

    public static boolean isPrime(int candidate) {
        int candidateRoot = (int) Math.sqrt((double) candidate);
        return IntStream.rangeClosed(2, candidateRoot)
                .noneMatch(i -> candidate % i == 0);
    }

}
