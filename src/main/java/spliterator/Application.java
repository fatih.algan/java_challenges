package spliterator;

import java.util.Spliterator;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class Application {

    private static int countWords(Stream<Character> stream) {
        WordCounter wordCounter = stream.reduce(new WordCounter(0, true),
                WordCounter::accumulate, WordCounter::combine);
        return wordCounter.getCounter();
    }

    public static void main(String[] args) {
        final String SENTENCE =
                " Nel   mezzo del cammin  di nostra  vita " +
                        "mi  ritrovai in una  selva oscura" +
                        " ché la  dritta via era   smarrita ";
        Stream<Character> stream = IntStream.range(0, SENTENCE.length()).mapToObj(SENTENCE::charAt);
        // Evidently something has gone wrong, but what? The problem isn’t hard to discover.
        // Because the original String is split at arbitrary positions,
        // sometimes a word is divided in two and then counted twice.
        System.out.println("Found " + countWords(stream.parallel()) + " words");
        //The solution consists of ensuring that the String isn’t split at a random position
        // but only at the end of a word. To do this, you’ll have to implement a Spliterator of
        // Character that splits a String only between two words
        //(as shown in the following listing) and then creates the parallel stream from it
        Spliterator<Character> spliterator = new WordCounterSpliterator(SENTENCE);
        stream = StreamSupport.stream(spliterator, true);
        System.out.println("Found " + countWords(stream) + " words");
    }
}
